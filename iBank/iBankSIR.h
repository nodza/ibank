//
//  iBankSIR.h
//  iBank
//
//  Created by Noel Hwande on 6/6/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface iBankSIR : NSObject

-(int) interestRateForLoan:(NSString *) amountString;
-(int) periodMonths:(NSString *) periodString;

-(int) investmentReturn:(int)investAmount investPeriod:(int)period;
-(int) investInterestAmount:(int)investAmount investPeriod:(int)period;

@end
