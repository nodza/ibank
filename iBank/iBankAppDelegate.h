//
//  iBankAppDelegate.h
//  iBank
//
//  Created by Noel Hwande on 6/4/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iBankAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
