//
//  iBankLoanViewController.h
//  iBank
//
//  Created by Noel Hwande on 6/4/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iBankLoanViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *amount;
@property (strong, nonatomic) IBOutlet UILabel *periodLabel;
@property (strong, nonatomic) IBOutlet UILabel *interestLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalPaymentLabel;

-(IBAction)submit:(UIButton *)sender;

@end
