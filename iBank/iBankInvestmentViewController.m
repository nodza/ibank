//
//  iBankInvestmentViewController.m
//  iBank
//
//  Created by Noel Hwande on 6/4/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import "iBankInvestmentViewController.h"
#import "iBankSIR.h"

@interface iBankInvestmentViewController ()

@end

@implementation iBankInvestmentViewController
@synthesize investTextField, periodTextField, interestLabel, returnLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)processFields
{
    iBankSIR *bank = [[iBankSIR alloc] init];
    [interestLabel setText:[NSString stringWithFormat:@"$%i",
                            [bank investmentReturn:[investTextField.text intValue] investPeriod:[periodTextField.text intValue]]]];
    [returnLabel setText:[NSString stringWithFormat:@"$%i",
                          [bank investmentReturn:[investTextField.text intValue] investPeriod:[periodTextField.text intValue]]]];
}

- (IBAction)submit:(UIButton *)sender {
    [self processFields];
}
@end
