//
//  iBankLoanViewController.m
//  iBank
//
//  Created by Noel Hwande on 6/4/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import "iBankLoanViewController.h"
#import "iBankSIR.h"

@interface iBankLoanViewController ()

@end

@implementation iBankLoanViewController
@synthesize amount, periodLabel, interestLabel, totalPaymentLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(IBAction)submit:(UIButton *)sender {
    [self process];
}

- (void)process
{
    iBankSIR *bank = [[iBankSIR alloc] init];
    char percentage = '%';
    int loanAmount = [amount.text intValue];
    int interestRate = [bank interestRateForLoan:amount.text];
    int months = [bank periodMonths:amount.text];
    int tInterest = loanAmount*interestRate/100*months/12;
    
    // Set labels
    self.interestLabel.text = [NSString stringWithFormat:@"%i%c",interestRate,percentage];
    self.periodLabel.text = [NSString stringWithFormat:@"%i months",months];
    self.totalPaymentLabel.text = [NSString stringWithFormat:@"$%i",(loanAmount +tInterest)];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
