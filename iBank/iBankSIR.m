//
//  iBankSIR.m
//  iBank
//
//  Created by Noel Hwande on 6/6/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import "iBankSIR.h"

@implementation iBankSIR

-(int) interestRateForLoan:(NSString *)amountString
{
    int amount = [amountString intValue];
    if (amount < 5000) {
        return 25;
    }
    if (amount < 1000) {
        return 15;
    }
    if (amount < 50000) {
        return 10;
    }
    else {
        return 8;
    }
}

-(int) periodMonths:(NSString *)periodString
{
    int amount = [periodString intValue];
    if (amount < 5000) {
        return 3;
    }
    if (amount < 10000) {
        return 12;
    }
    if (amount < 50000) {
        return 24;
    }
    else
        return 36;
}

-(int)investInterestAmount:(int)investAmount investPeriod:(int)period
{
    return (investAmount*6/100*period/12);
}

-(int)investmentReturn:(int)investAmount investPeriod:(int)period
{
    int endValue = investAmount + (investAmount*6/100*period/12);
    return endValue;
}

@end
