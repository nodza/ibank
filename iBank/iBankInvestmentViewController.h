//
//  iBankInvestmentViewController.h
//  iBank
//
//  Created by Noel Hwande on 6/4/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iBankInvestmentViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *investTextField;
@property (strong, nonatomic) IBOutlet UITextField *periodTextField;
@property (strong, nonatomic) IBOutlet UILabel *interestLabel;
@property (strong, nonatomic) IBOutlet UILabel *returnLabel;

- (IBAction)submit:(UIButton *)sender;

@end
